package mt.basicfunctions;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;
import java.io.IOException;

import mt.basicfunctions.SharedPreference.SaveInSharedPreference;
import mt.basicfunctions.basic.CheckInternet;
import mt.basicfunctions.basic.MyMethods;
import mt.basicfunctions.basic.SerializationUtil;

/**
 * Created by Talib on 8/28/2015.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText name;
    private Button send;
    private CheckInternet checkInternet;
    private SaveInSharedPreference savePrefs;
    private MyMethods myMethods;
    private Activity activity;
    public static File userFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        myMethods = MyMethods.getInsance();
        myMethods.fullScreen(activity);

        setContentView(R.layout.activity_main);

        initValues();

       // getSerialize();

        //make folder
        myMethods.createDirIfNotExists("talib");

    }
    //onCreat close

    //initializing values start
    public void initValues(){

        savePrefs = SaveInSharedPreference.getInstance();
        name = (EditText)findViewById(R.id.name);
        myMethods.textWatcherCapitalWord(name);
        send = (Button)findViewById(R.id.send);
        send.setOnClickListener(this);


    }
    //inititalizing values end


    //onClick listerner end
    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.send:{

                if(CheckInternet.getInstance().checkInternet(this)){
                    //do your work

                    if(!name.getText().toString().trim().isEmpty()){

                    }else{
                     name.setBackgroundResource(R.drawable.edit_invalid);
                    }

                }else{
                    myMethods.showToast(activity, "Please connect internet!");
                }

                break;
            }

        }

    }
    //onClick listener end


   public void getSerialize(){
       userFile = new File(getFilesDir(),"user.ser");

        //for serialize
        // SerializationUtil.serialize(new Gson().toJson("class name/ model class"), userFile);


        //for deserialize
        try {
            String serializeString = (String) SerializationUtil.deserialize(userFile);
            //user = new Gson().fromJson(serializeString, Profile.class);
            // user will be the class name or model
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }



}
